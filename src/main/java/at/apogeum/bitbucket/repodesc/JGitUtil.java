package at.apogeum.bitbucket.repodesc;

import at.apogeum.bitbucket.repodesc.config.RepoConfig;
import at.apogeum.bitbucket.repodesc.rest.data.RepositoryData;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.user.ApplicationUser;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Uses the eclipse JGit library to enrich RepoConfig objects with data from each repository's latest
 * commit.
 */
public class JGitUtil {

    private static final Logger LOG = LoggerFactory.getLogger(JGitUtil.class);

    // Global cache of FileRepository objects because they are a bit slow to create, and use very little memory.
    private static final Map<Integer, FileRepository> JGITS = new ConcurrentHashMap<Integer, FileRepository>();

    /**
     * Given a list of repoConfigs, return a list of RepositoryData objects
     * filled with data from the most recent commit in the given repo.
     * <p>
     * Note:  we only look at branch tips.  Tag tips that are not also attached to a branch are ignored
     * as far as calculating the most recent commit in a repo.
     *
     * @param repoConfigs RepoConfig objects to enrich with this additional data.
     * @return list of RepositoryData objects enriched with data from the most recent commit in the given repo.
     */
    public static List<RepositoryData> joinWithCommitData(
            RepositoryService repoService, PermissionService permService, ApplicationPropertiesService propsService,
            ApplicationUser currentUser, List<RepoConfig> repoConfigs
    ) {
        List<RepositoryData> result = new ArrayList<RepositoryData>(repoConfigs.size());
        for (RepoConfig rc : repoConfigs) {
            FileRepository fr = JGITS.get(rc.getRepoId());
            if (fr == null) {
                Repository repo = repoService.getById(rc.getRepoId());
                if (repo != null) {
                    File path = propsService.getRepositoryDir(repo).getAbsoluteFile();
                    String gitDir = path.getAbsolutePath();
                    try {
                        fr = new FileRepository(gitDir);
                        JGITS.put(repo.getId(), fr);
                    } catch (IOException ioe) {
                        LOG.warn("JGit failed to read " + gitDir + " " + ioe, ioe);
                    }
                }
            }
        }

        // For now we don't bother dividing the work into parallel batches.
        // Just do all the work in the current thread:
        JoinTask task = new JoinTask(repoConfigs, permService, currentUser);
        task.run();
        result.addAll(task.getResult());
        return result;
    }

    private static class Commit {
        private final long when;
        private final String msgShort;
        private final String msgFull;
        private final String who;

        Commit(long when, String msgShort, String msgFull, String who) {
            this.when = when;
            this.msgShort = msgShort;
            this.msgFull = msgFull;
            this.who = who;
        }

        Long getWhen() {
            return when;
        }

        String getMsgShort() {
            return msgShort != null ? msgShort.trim() : "";
        }

        String getMsgFull() {
            return msgFull != null ? msgFull.trim() : "";
        }

        String getWho() {
            return who != null ? who.trim() : "";
        }
    }

    /**
     * Does the actual work of fetching the commit data
     * and creating the list of RepositoryData objects.
     */
    private static class JoinTask implements Runnable {
        private List<RepoConfig> data;
        private List<RepositoryData> result;
        private PermissionService permService;
        private ApplicationUser currentUser;

        JoinTask(
                List<RepoConfig> data,
                PermissionService permService,
                ApplicationUser currentUser
        ) {
            this.data = data;
            this.permService = permService;
            this.currentUser = currentUser;
        }

        public void run() {
            this.result = join(permService, currentUser, data);
        }

        List<RepositoryData> getResult() {
            return result;
        }
    }

    private static List<RepositoryData> join(
            PermissionService permService, ApplicationUser currentUser, List<RepoConfig> repoConfigs) {

        List<RepositoryData> result = new ArrayList<RepositoryData>(repoConfigs.size());
        for (RepoConfig rc : repoConfigs) {

            String lastUpdate = "";
            String lastMessage = "";
            String lastMessageFull = "";
            String lastAuthor = "";
            Commit c = null;
            try {
                Integer id = rc.getRepoId();
                FileRepository fr = JGITS.get(id);
                if (fr != null) {
                    // Only grab commit data if current user has permission to see it.
                    boolean userCanRead = permService.hasRepositoryPermission(currentUser, id, Permission.REPO_READ);
                    userCanRead = userCanRead || permService.isRepositoryAccessible(id);
                    if (userCanRead) {
                        c = getMostRecentCommit(fr);
                    }
                }
                if (c != null) {
                    lastUpdate = c.getWhen().toString();
                    lastMessage = c.getMsgShort();
                    lastMessageFull = c.getMsgFull();
                    lastAuthor = c.getWho();
                }
            } catch (IOException ioe) {
                LOG.warn("Cannot fetch most recent commit from repo: " + ioe, ioe);
            }
            result.add(new RepositoryData(
                    rc.getSlug(), rc.getDescription(), lastUpdate, lastMessage, lastMessageFull, lastAuthor
            ));
        }
        return result;
    }

    /**
     * Simulates a "git log --date-order --branches -1" call using JGit.
     *
     * @param fileRepo JGit FileRepository object to query.
     * @return The newest commit object from the repository.
     */
    private static Commit getMostRecentCommit(FileRepository fileRepo) throws IOException {

        Git git = new Git(fileRepo);
        long newest = -1;
        RevCommit newestCommit = null;

        // Poor woman's "git log --branches -1".
        Map<String, Ref> refs = git.getRepository().getRefDatabase().getRefs("refs/heads/");
        RevWalk walk = new RevWalk(fileRepo);
        for (Ref ref : refs.values()) {
            ObjectId objectId = ref.getObjectId();
            RevCommit commit;
            try {
                commit = walk.parseCommit(objectId);
                PersonIdent committer = commit.getCommitterIdent();
                long committerTime = committer.getWhen().getTime();
                if (committerTime > newest) {
                    newest = committerTime;
                    newestCommit = commit;
                }
            } catch (MissingObjectException e) {
                // ignore
            } catch (IncorrectObjectTypeException e) {
                // ignore
            }
        }
        walk.dispose();

        if (newestCommit != null) {
            PersonIdent author = newestCommit.getAuthorIdent();
            long authorTime = author.getWhen().getTime();
            long commitTime = newestCommit.getCommitterIdent().getWhen().getTime();
            String shortMsg = newestCommit.getShortMessage();
            String fullMsg = newestCommit.getFullMessage();

            String n = author.getName();
            if (author.getEmailAddress() != null) {
                n += " <" + author.getEmailAddress() + ">";
            } else {
                n += " <>";
            }
            return new Commit(Math.max(authorTime, commitTime), shortMsg, fullMsg, n);
        }

        // empty repo
        return null;
    }

}