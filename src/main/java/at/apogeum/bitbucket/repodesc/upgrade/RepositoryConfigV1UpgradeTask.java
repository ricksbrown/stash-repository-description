package at.apogeum.bitbucket.repodesc.upgrade;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
public class RepositoryConfigV1UpgradeTask implements ActiveObjectsUpgradeTask {

    public RepositoryConfigV1UpgradeTask() {
    }

    @Override
    public ModelVersion getModelVersion() {
        return ModelVersion.valueOf("1");
    }

    @Override
    public void upgrade(ModelVersion modelVersion, ActiveObjects activeObjects) {
        if (!modelVersion.isSame(ModelVersion.valueOf("0"))) {
            throw new IllegalStateException("RepositoryConfigV1UpgradeTask can only upgrade from version 0");
        }

    }
}
