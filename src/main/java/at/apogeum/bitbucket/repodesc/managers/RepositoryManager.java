package at.apogeum.bitbucket.repodesc.managers;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
public class RepositoryManager {
    
    private final RepositoryService repositoryService;

    public RepositoryManager(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }
    
    public void findByProjectKey(String projectKey, int start, int limit, RepositoryCallback callback) {
        PageRequest pageRequest = new PageRequestImpl(start, limit);
        Page<? extends Repository> repositories = repositoryService.findByProjectKey(projectKey, pageRequest);
        for (Repository repository : repositories.getValues()) {
            callback.handle(repository);
        }
    }
    
    public void findPublic(int start, int limit, RepositoryCallback callback) {
        PageRequest pageRequest = new PageRequestImpl(start, limit);
        Page<? extends Repository> allRepos = repositoryService.findAll(pageRequest);
        for (Repository repository : allRepos.getValues()) {
            if (repository.isPublic()) {
                callback.handle(repository);
            }
        }
    }
    
    public void findPrivate(String userId, int start, int limit, RepositoryCallback callback) {
        PageRequest pageRequest = new PageRequestImpl(start, limit);
        String projectKey = "~" + userId.toUpperCase();
        Page<? extends Repository> repositories = repositoryService.findByProjectKey(projectKey, pageRequest);
        for (Repository repository : repositories.getValues()) {
            callback.handle(repository);
        }
    }
}
